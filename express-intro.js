import express from "express"
import path from "path"
import { fileURLToPath } from "url"

/*
	Da wir unser script als Modul ausführen, existiert es in einem eigenen Scope.
	Daher haben wir keine Zugriff auf die Variablen __filename und __dirname.
	Wir erstellen diese nun selbst. Wichtig ist, dass dies in einem Script geschieht,
	das im der Projekt root liegt. Beide Konstanten beinhalten absolute Pfade.
	Diese sind nötig, wenn wir mit dem Filesystem arbeiten.
*/
const __filename = fileURLToPath(import.meta.url)
// Finde das aktuelle Verzeichnis anhand des aktuellen Dateipfads
const __dirname = path.dirname(__filename)

// aktuellen Process beenden
// process.exit()
const port = 3000
const host = "localhost"
// Framework erstellen
const app = express()

function getView(file) {
	return path.join(__dirname, "views", file)
}

/*
	Routes:
	Mapping von URLs und deren Methods auf functions

	Über den Aufruf einer route rufen wir (Controller) functions auf
*/
app.get("/", (req, res) => {
	// Wir zeigen ein zu ladendes Dokument
	res.sendFile(getView("index.html"))
})

app.get("/shop", (req, res) => {
	res.sendFile(getView("shop.html"))
})

app.listen(port, () => {
	console.log(`Server läuft unter http://${host}:${port}`)
})