import express from "express"
import path from "path"
import { fileURLToPath } from "url"
import fs from "fs"

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

const port = 3000
const host = "localhost"
const app = express()

// express unsere template engine bekannt geben
app.set("view engine", "ejs")

app.get("/", (req, res) => {
	/*
		ejs geht in der default Konfiguration von einem Ordner namens
		views im project root Verzeichnis aus.
		Dateien werden ohne Endung angegeben.
		Die default Datei Endung lautet .ejs
	*/
	const date = new Date()
	const data = {
		title: "Homepage",
		mainHeading: "Herzlich willkommen",
		content: "<p>Hier sollte eine Menge Text stehen</p>",
	}

	res.render("index", {
		pageVars: data,
		date: date
	})
})

app.get("/shop", (req, res) => {
	res.render("shop")
})

app.get("/customers", (req, res) => {
	// read customers json
	fs.readFile("./data/customers.json", "utf8", (error, data) => {
		if (error) {
			res.send("<p>loading error</p>")
			return
		}

		const pageVars = {
			title: "Customers",
			mainHeading: "Show all customers",
			content: "<p>Customers table here</p>",
		}
		const customers = JSON.parse(data)

		res.render("customers", {
			pageVars: pageVars,
			customers: customers
		})
	})
})

app.get("/admin", (req, res) => {
	// Wir können auch Unterordner in views aufrufen
	// Unsere Templates gehen immer vorm views Verzeichnis aus
	res.render("admin/admin")
})

app.listen(port, () => {
	console.log(`Server läuft unter http://${host}:${port}`)
})